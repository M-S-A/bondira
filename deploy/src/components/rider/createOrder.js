import React ,{Component} from 'react'
import { Form, Input, Tooltip, Icon, Cascader, message, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import firebase from 'firebase'
import {browserHistory} from 'react-router'
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;


class SimpleForm extends React.Component {
    state = {
    };
    handleSubmit = (e) => {
      e.preventDefault();
      this.props.form.validateFieldsAndScroll((err, values) => {
        if (!err) {
          console.log('Received values of form: ', values);
          var pushID = firebase.database().ref().child("orders").push().getKey();
          console.log(pushID,"000000000000000000000")
          firebase.database().ref().child("orders").child(pushID).set({
              currency:values.prefix,
              to:values.to,
              from:values.from,
              cost:values.cost,
              mustHave:values.mustHave,
              preferToHave:values.preferToHave,
              riderUID:firebase.auth().currentUser.uid,
              status:"NEW"  ,
              postedAt:Date.now(),
              orderID : pushID
          },()=>{this.props.form.resetFields();
            message.success(" تم انشاء الطلب بنجاح");
            browserHistory.push('/')
          })
        }
      });
    }
  
    render() {
      const { getFieldDecorator } = this.props.form;
  
      const formItemLayout = {
        labelCol: {
          xs: { span: 24 },
          sm: { span: 8 },
        },
        wrapperCol: {
          xs: { span: 24 },
          sm: { span: 16 },
        },
      };
      const tailFormItemLayout = {
        wrapperCol: {
          xs: {
            span: 24,
            offset: 0,
          },
          sm: {
            span: 16,
            offset: 8,
          },
        },
      };
      const prefixSelector = getFieldDecorator('prefix', {
        initialValue: 'E£',
      })(
        <Select style={{ width: 70 }}>
          <Option value="E£">E£</Option>
        </Select>
      );
      return (
        <div
        style={{
          justifyContent:"center"
        }}
        >
        <Form onSubmit={this.handleSubmit} className="business-form">
          <FormItem
            label={(
              <span style={{color:"white"}}>
                إلى
              </span>
            )}
          >
            {getFieldDecorator('to', {
              rules: [{ required: true, message: 'يرجى إدخال موقع الوجهة!', whitespace: true }],
            })(
              <Input 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                من
                </span>
              )}
          >
            {getFieldDecorator('from', {
              rules: [{ required: true, message: 'يرجى تحديد نقطة الاستلام' }],
            })
            (
              <Input 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                كلفة
                </span>
              )}
          >
            {getFieldDecorator('cost', {
              rules: [{ required: true, message: 'المبلغ المستحق ' }],
            })(
              <Input addonAfter={prefixSelector} style={{ width: '100%' }}
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
                <span style={{color:"white"}}>
                يجب ان يملك
                </span>
              )}
          >
            {getFieldDecorator('mustHave', {
              rules: [{ required: true, message: 'يجب ان يملك' }],
            })(
              <Input style={{ width: '100%' }} 
              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>          
          <FormItem
            label={(
                <span style={{color:"white"}}>
                تفضل أن يكون
                </span>
              )}
          >
            {getFieldDecorator('preferToHave', {
              rules: [{ required: true, message: 'تفضل أن يكون' }],
            })(
              <Input style={{ width: '100%' }} 

              style={{
                textAlign:"right"
              }}
              />
            )}
          </FormItem>
          <FormItem
                      label={(
                        <span style={{color:"white"}}>
                        تفاصيل
                        </span>
                      )}        
          >
            {getFieldDecorator('details', {
              rules: [{  message: 'يرجى تقديم مزيد من التفاصيل الخاصة بك' }],
            })(
              <Input.TextArea
              style={{borderRadius:25,textAlign:"right",backgroundColor:"#626268",color:"white"}}
              rows="7"
              placeholder="********* تذكر دائما ، إعطاء مزيد من التفاصيل للحصول على سائقي الشاحنات أفضل. ***********
يرجى تقديم المزيد من التفاصيل. فمثلا:
1- في أي وقت تريد السيارة؟
2- ما نوع البضائع التي تريد نقلها؟
3- هل تحتاج إلى مساعدة في تحميل أو تفريغ البضائع؟
4- أي احتياجات خاصة للبضائع مثل هل تحتاج إلى ثلج للأسماك؟
أو
5- هل تحتاج إلى حامل زجاج؟
6- ثلاجات؟
7- ما مدى ثقل الشحنة؟
8- كم مرة ستتوقف الشاحنة من المصدر إلى الوجهة؟"
              />          )}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
          <Button ghost htmlType="submit">انشاء الطلب</Button>
          </FormItem>
        </Form>
        </div>
      );
    }
  }
  
  
const CreateOrder = Form.create()(SimpleForm);
export default CreateOrder