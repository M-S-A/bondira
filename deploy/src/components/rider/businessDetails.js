import React, { Component } from 'react';

import styles from '../../styles/riderBusiness.css'
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete } from 'antd';
import { browserHistory } from 'react-router'
import firebase from 'firebase'
const FormItem = Form.Item;
const Option = Select.Option;


class BusinessDetail extends React.Component {
  constructor(){
    super();
    this.state={
      validateStatus1:"success",
      help1:"",
      validateStatus2:"success",
      help2:""
    }
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        firebase.database().ref("user" + "/" + firebase.auth().currentUser.uid).update(
          JSON.parse(JSON.stringify({ "businessDetail": values }))
        ).then(() => {
          firebase.database().ref("user" + "/" + firebase.auth().currentUser.uid).child("type").once("value",(snap)=>{
            var userType = snap.val();
            browserHistory.push(`/${userType}`)
          })
        }
      )
      }
    });
  }
  componentDidMount(){
    firebase.auth().onAuthStateChanged((user)=>{
      if(!user){browserHistory.push('/')}
    })
  }
  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 },
      },
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0,
        },
        sm: {
          span: 16,
          offset: 8,
        },
      },
    };

    return (
      <div
        style={{
          justifyContent: "center"
        }}
      >
        <Form onSubmit={this.handleSubmit} className="business-form">
        <FormItem>
              {getFieldDecorator('name', {
                rules: [{ required: true, message: 'يرجى إدخال اسمك الكامل!' }],
              })(
                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} type="text"  placeholder="اسمك الكامل"
                style={{color:"white"}}
                />
              )}
            </FormItem>
          <FormItem
            label={(
              <span
                style={{ color: "white" }}
              >
                Business Name&nbsp;
              <Tooltip title="اسم عملك">
                  <Icon type="question-circle-o" />
                </Tooltip>
              </span>
            )}
          >
            {getFieldDecorator('businessName', {
              rules: [{ required: true, message: 'اسم عملك - إذا أمكن ذلك', whitespace: true }],
            })(
              <Input
                style={{
                  textAlign: "right"
                }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                Business Address
            </span>
            )}
          >
            {getFieldDecorator('address', {
              rules: [{ required: true, message: 'يرجى تحديد عنوان عملك' }],
            })
              (
              <Input
                style={{
                  textAlign: "right"
                }}
              />
              )}
          </FormItem>
          <FormItem
            validateStatus={this.state.validateStatus1}
            help={this.state.help1}
            label={(
              <span style={{ color: "white" }}>
                Phone Number 1
            </span>
            )}
          >
            {getFieldDecorator('phone1', {
              rules: [{ required: true, message: 'يرجى إدخال رقم هاتفك!' }],
            })(
              <Input style={{ width: '100%' }}
                type="tel"
                maxLength={13}
                onChange={(event) => {
                  // this.setState({phone:event.target.value})
                  event.target.value.match(/^[0-9]+$/) ?
                    this.setState({ validateStatus1: "success", help1: "" }) :
                    this.setState({ validateStatus1: "error", help1: "يرجى إدخال رقم هاتفك على سبيل المثال  00201231234567" })
                }}
                style={{
                  textAlign: "right"
                }}
              />
            )}
          </FormItem>
          <FormItem
            validateStatus={this.state.validateStatus2}
            help={this.state.help2}
            label={(
              <span style={{ color: "white" }}>
                Phone Number 2
            </span>
            )}
          >
            {getFieldDecorator('phone2', {
              rules: [{ message: 'يرجى إدخال رقم هاتفك!' }],
            })(
              <Input
                type="tel"
                maxLength={13}
                onChange={(event) => {
                  // this.setState({phone:event.target.value})
                  event.target.value.match(/^[0-9]+$/) ?
                    this.setState({ validateStatus2: "success", help2: "" }) :
                    this.setState({ validateStatus1: "error", help2: "يرجى إدخال رقم هاتفك على سبيل المثال  00201231234567" })
                }}
                style={{ width: '100%' }}
                style={{
                  textAlign: "right"
                }}
              />
            )}
          </FormItem>
          <FormItem
            label={(
              <span style={{ color: "white" }}>
                General Information
            </span>
            )}
          >
            {getFieldDecorator('generalInformation', {
              rules: [{ message: 'يرجى تقديم تفاصيل معلومات الأعمال العامة الخاصة بك' }],
            })(
              <Input.TextArea
                style={{ borderRadius: 25, textAlign: "right", backgroundColor: "#626268", color: "white" }}
                rows="7"
                placeholder="أظهر لعملائك ما تفعله.
خدماتك.
أوقاتك.
السيارات الخاصة بك.
احتياجاتك.
تفاصيل."
              />)}
          </FormItem>
          <FormItem {...tailFormItemLayout}>
            <Button ghost htmlType="submit">تسجيل</Button>
          </FormItem>
        </Form>
      </div>
    );
  }
}

const RiderBusiness = Form.create()(BusinessDetail);

export default RiderBusiness