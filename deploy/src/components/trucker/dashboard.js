import React from 'react';
import {Tabs, Tab,} from 'material-ui/Tabs';
import Market from './market'
import AcceptedOrder from './acceptedOrder'
import {Affix} from 'antd'
import SwipeableViews from 'react-swipeable-views';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400,
  },
  slide: {
    height:"100%",
    padding: 10,
  },
};

export default class TruckerDashboard extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0,
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  render() {
    const fontSize = window.innerWidth <600 ? 10 :15    
    return (
      <div>
        <Affix offsetTop={108}>
        <Tabs
          onChange={this.handleChange}
          value={this.state.slideIndex}

        >
          <Tab label="Accepted Order" value={0} 
                    style={{
                        backgroundColor:"#636068",
                        fontSize:fontSize      
                        
                    }}
            />
          <Tab label="Market" value={1}
                    style={{
                        backgroundColor:"#636068",
                        fontSize:fontSize      
                        
                    }} 
            />
        </Tabs>
        </Affix>
        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
          <div>
            <AcceptedOrder/>
          </div>
          <div style={styles.slide}>
          <Market/>
          </div>
        </SwipeableViews>
      </div>
    )
  }
}   