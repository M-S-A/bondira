import React,{Component} from 'react'
import {Form,message,Icon,Input,InputNumber,Button} from 'antd'
import firebase from 'firebase'
import styles from '../../styles/signupPhone.css';
import {browserHistory} from 'react-router'
const FormItem = Form.Item;
class SignInPhone extends Component{
  constructor(){
    super();
    this.state={
      validateStatus:"success",
      help:""
    }
  }
//-------------------------------------------------------
handleSubmit = (e) => {
  e.preventDefault();
  this.props.form.validateFields((err, values) => {
    console.log(values,"values from field decorator")
    var phoneNum = values.phone.split("");
    while(phoneNum[0]==0){phoneNum.shift()}
    phoneNum = phoneNum.join("")
    console.log(phoneNum,"phonenum")    
    if (!err && this.state.validateStatus === "success") {
      window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
        'size': 'normal',
        'callback': function(response) {
          firebase.auth().signInWithPhoneNumber(`+${phoneNum}`,window.recaptchaVerifier).then(
            (confirmationResult)=>{
              window.confirmationResult=confirmationResult;
            console.log(window.confirmationResult,"this is magic")
            browserHistory.push("/verifyCode")  
            }
          )
        },
        'expired-callback': function() {
          alert("يرجى المحاولة مرة أخرى - 1")
        }
      }) 

    window.recaptchaVerifier.render().then(function(widgetId) {
    window.recaptchaWidgetId = widgetId;

    });

    }
    else {message.error(" خطأ! يرجى إدخال رقم الهاتف الصحيح ---- مثل 00201012345678")}
  });
}
//----------------------------------------------------------

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} className="login-form">
      <FormItem
      validateStatus={this.state.validateStatus}
      help={this.state.help}
      >{
        getFieldDecorator(
          'phone', {
            rules: [{ required: true, message: 'يرجى إدخال رقم هاتفك' }],
          }
        )(
          <Input
          prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
          placeholder="رقم الهاتف على سبيل المثال: 00201012345678 "
          type="tel"
          onChange={(event)=>{
            event.target.value.match(/^[0-9]+$/)?
            this.setState({validateStatus:"success",help:""}):
            this.setState({validateStatus:"error",help:"يرجى إدخال رقم هاتفك على سبيل المثال  00201231234567"})
          }}
          />)
        }
      </FormItem>
      <FormItem>
          <Button ghost htmlType="submit" className="login-form-button"
          style={{display:this.state.visiblility}}
          >
            ارسل الرقم السري
          </Button>

          <br/>
      </FormItem>
      <div id="recaptcha-container"></div>
      </Form>
    )
  }
} 
const SignInWithPhone = Form.create()(SignInPhone);
export default SignInWithPhone