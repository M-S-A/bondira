import React,{Component} from 'react'
import { Menu, Icon, Button } from 'antd';
const SubMenu = Menu.SubMenu;

class Settings extends React.Component {
  state = {
    collapsed: true,
    visible:false
  }
  toggleCollapsed = () => {
    this.setState({
      visible: !this.state.visible,
    });
  }
  render() {
    return (
      <div style={{ width: 256 }}>
        <Button type="ghost" onClick={this.toggleCollapsed} style={{ marginBottom: 16 }}>
          <Icon type={this.state.visible ? 'menu-unfold' : 'menu-fold'} />
        </Button>
        <Menu style={{visibility:this.state.visible?"visible":"hidden"}}
          defaultSelectedKeys={['1']}
          defaultOpenKeys={['sub1']}
          mode="vertical"//"inline"
          theme="dark"
          inlineCollapsed={this.state.collapsed}
        >
          <Menu.Item key="1">
            <Icon type="pie-chart" />
            <span>Option 1</span>
          </Menu.Item>
          <Menu.Item key="2">
            <Icon type="desktop" />
            <span>Option 2</span>
          </Menu.Item>
          <Menu.Item key="3">
            <Icon type="inbox" />
            <span>Option 3</span>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}
export default Settings