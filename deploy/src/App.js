import React, { Component } from 'react';
import {Provider} from 'react-redux'
import './App.css';
import store from './store'
import Navigation  from './navigation'


//////////////////////////////to be removed/////////////////
// import Header  from './components/header'
// import Dashboaord from './containers/dashboard'
// import RegisterEmail  from './containers/registerEmail'
import SignUpWithPhone from './components/auth/signupPhone'
// import LoginEmail from './containers/LoginEmail'
//////////////////////////////to be removed/////////////////



class App extends Component {
  
  constructor(){
    super()
  }
  render() {
    
    return (
      // <Dashboaord/>
      // <LoginEmail/>
      // <Header/>
      // <RegisterEmail/>
      // <SignUpWithPhone/>
      <Provider store={store}>
      <Navigation />
      </Provider>
    );
  }
}





export default App;
