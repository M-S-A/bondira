import React ,{Component} from 'react'
import {Header,Profile} from '../components'
class ProfileComponent extends Component{
    render(){
        return(
            <div
            style={{
                backgroundColor:"#41404c",
                height:"100vh"
            }}
            >
            <Header/>
            <br/>
            <Profile id={this.props.routeParams.uid}/>
            </div>
        )
    }
}
export default ProfileComponent